﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	Rigidbody rbProjectile;
	public AudioClip[] glassShatters;
	public AudioClip blop;
	public AudioClip shatter;
	AudioSource audioSource;
	public bool alreadyCollided = false;
	public Renderer rend;
	public MeshCollider sphcol;

	//	public Material[] mat;
	public bool isExploding = false;
	public int currentlyCollidingSame = 0;
	public List<Transform> allSameColliding = new List<Transform>();

	//shattered
	public GameObject shattered;
	Component[] shatteredMat;

	//camera script/object
	public CameraShake camShake;
	public GameObject cam;


	// Use this for initialization
	void Start () {
		rbProjectile = GetComponent<Rigidbody> ();

		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = blop;
		//		int chosenMaterial = Random.Range (0,mat.Length);
		//		GetComponent<Renderer>().material = mat [chosenMaterial];


	}

	// Update is called once per frame
	void Update () {
		//debug input
		if(Input.GetKey(KeyCode.X)){

		}
	}

	void OnCollisionEnter(Collision col){
		//make it static if its not colliding with a fragment
		if(rbProjectile.isKinematic == false){
			if(col.gameObject.tag == "ball" || col.gameObject.tag == "pivotPoint"){
			gameObject.transform.SetParent(col.gameObject.transform);
			rbProjectile.isKinematic = true;
			//alreadyCollided = true;
			}
		}
		//if the ball is not of the same material and has already colllide, increment the score
		if (GetComponent<Renderer> ().material.name != col.transform.GetComponent<Renderer> ().material.name && alreadyCollided == false &&col.gameObject.tag != "fragment" ) {
			PaddleController.score ++;
		}

		//set the new ball as a child to other parent ball
		if(col.gameObject.tag == "ball" && rbProjectile.isKinematic == false){
			Debug.Log ("ball!");
			//gameObject.transform.parent = col.gameObject.transform;
			//col.gameObject.transform.SetParent(gameObject.transform);
			alreadyCollided = true;
		}

		//make the other colliding ball a child of the current ball
		if(alreadyCollided == true){
			//Debug.Log ("ball!");
			//col.gameObject.transform.parent = transform;
			//gameObject.transform.SetParent(col.gameObject.transform);
		}

		/*if(col.gameObject.tag != "ball"){
			//register that the object has collided with another object
			alreadyCollided = true;
		}*/

		//if balls collide with wall, increase the score
		if(col.gameObject.tag == "wall"){
			destroyItself ();
		}
			
//		if(col.gameObject.tag == "outline"){
//			Debug.Log ("destroyed!");
//			PaddleController.score ++;
//			ChangeChildMaterial ();
//			Instantiate (shattered,transform.position,transform.rotation);
//			Destroy(gameObject);
//		}
//
//		Debug.Log ("hit!");

//		rbProjectile.constraints = RigidbodyConstraints.FreezePosition;
//		rbProjectile.constraints = RigidbodyConstraints.FreezeAll;
		audioSource = GetComponent<AudioSource>();
		audioSource.PlayOneShot(glassShatters[1]);
		//GetComponent<AudioSource> ().Play ();

		if(col.transform.tag == "ball"){

			if(GetComponent<Renderer>().material.name == col.transform.GetComponent<Renderer>().material.name){
				Debug.Log("same Material!");
				currentlyCollidingSame++;
				allSameColliding.Add (col.transform);

				if (currentlyCollidingSame > 1) {
					//tell others to explode
					//explode myself
					Explode();
				}
			}
		}
		//change to null if its a wall so it doesnt count score
		if(col.gameObject.tag == "wall" || col.gameObject.tag == "null"){
			gameObject.tag = "null";
		}
	}
		

	void Explode(){
//		Debug.Log ("Time to explode!");
		isExploding = true;
		foreach (Transform ball in allSameColliding) {
			if (ball.GetComponent<Projectile> ().isExploding == false) {
				ball.GetComponent<Projectile> ().Explode ();
			}
		}

		//assign color to shattered object and destroy itself
		ChangeChildMaterial ();
		destroyItselfWithPots ();
	}

	public void ChangeChildMaterial(){
		Renderer[] children;
		children = shattered.GetComponentsInChildren<Renderer> ();
		foreach(Renderer rend in children){
			rend.material = gameObject.GetComponent<Renderer> ().material;
		}
	}

	public void destroyItself(){
		Instantiate (shattered,transform.position,transform.rotation);
		PaddleController.score++;
		cam = GameObject.Find ("Camera");
		camShake = cam.GetComponent<CameraShake> ();
		camShake.shatterCameraShake ();
		Destroy (gameObject);
	}

	public void destroyItselfWithPots(){
		Instantiate (shattered,transform.position,transform.rotation);
		PaddleController.score--;
		cam = GameObject.Find ("Camera");
		camShake = cam.GetComponent<CameraShake> ();
		camShake.shatterCameraShake ();
		Destroy (gameObject);
	}

}