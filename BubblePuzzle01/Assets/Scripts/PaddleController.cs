﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaddleController : MonoBehaviour {
	//rigidbody
	public Rigidbody rbPaddle;

	//paddle variables
	public float horizontalSpeed;
	public float moveSpeed;
	public float projectileSpeed;
	public float torque;
	public float fireRate = 1.0f;
	private float NextFire;

	// Use this for initialization
	public Transform paddleTop;

	//gameobject array
	public GameObject[] projectile;
	public Material[] mat;

	//score
	public Text countText;
	public int startScore;
	public static int score;

	void Start () {
		rbPaddle = GetComponent<Rigidbody> ();
		score = startScore;

		changeColor ();
	}

	// Update is called once per frame
	void Update () {
		//score = Projectile.count;
		countText.text = score.ToString ();

		//left and right rotation
		if(Input.GetKey(KeyCode.LeftArrow)) {
			//rotates relative to the world
			transform.Rotate (new Vector3(0,0,moveSpeed) * Time.deltaTime, Space.World);
		}

		if(Input.GetKey(KeyCode.RightArrow)) {
			transform.Rotate (new Vector3(0,0,-moveSpeed) * Time.deltaTime, Space.World);
		}

		//Shoots
		if(Input.GetKeyDown(KeyCode.Space) && Time.time > NextFire){
			GameObject ballInstance;
			ballInstance = Instantiate (projectile[Random.Range(0,projectile.Length)], paddleTop.position, Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))) ) as GameObject;
			ballInstance.GetComponent<Renderer> ().material = GetComponent<Renderer> ().material;

			ballInstance.GetComponent<Rigidbody>().AddForce (paddleTop.up * projectileSpeed);
			float turn = Input.GetAxis("Horizontal");
			ballInstance.GetComponent<Rigidbody> ().AddTorque (transform.up * torque * turn);
			changeColor ();

			GetComponent<AudioSource> ().Play(); 

			NextFire = Time.time + fireRate;
		}

		//line that point to the direction that you are shooting
//		Vector3 forward = transform.TransformDirection(Vector3.up) * 10;
//		//Debug.DrawRay(transform.position, forward, Color.red);

		//angle restrict

		//		Vector3 currentRotation = transform.localRotation.eulerAngles;
		//		currentRotation.z = Mathf.Clamp(currentRotation.z, minAngleLimit, maxAngleLimit);
		//		transform.localRotation = Quaternion.Euler (currentRotation);
		//		Debug.Log (currentRotation);
	}

	public void changeColor(){
		int chosenMaterial = Random.Range (0,mat.Length);
		GetComponent<Renderer>().material = mat [chosenMaterial];
	}
}