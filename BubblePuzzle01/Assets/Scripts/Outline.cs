﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outline : MonoBehaviour {

	public Projectile projectileScript;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {

		/*if(other.gameObject.tag == "ball"){
			Debug.Log ("destroyed!");
			PaddleController.score ++;
			Destroy(other.gameObject);
		}*/
		Debug.Log ("destroyed!");
		other.transform.parent = null;
		//other.transform.DetachChildren();
		other.GetComponent<Rigidbody> ().isKinematic = false;

	}

	void OnCollisionEnter(Collision col){
		projectileScript = col.gameObject.GetComponent<Projectile> ();
		projectileScript.ChangeChildMaterial ();
		projectileScript.destroyItself ();

	}
}
