﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneAudioPlay : MonoBehaviour {

	public AudioClip[] glassShatters;
	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayExplosionSound(){
		audioSource = GetComponent<AudioSource> ();
		audioSource.Play ();
	}
}
