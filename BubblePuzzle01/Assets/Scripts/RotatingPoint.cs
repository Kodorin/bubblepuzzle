﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPoint : MonoBehaviour {
	public float rSpeed;
	static public float rotatingSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.tag == "light") {
			rotatingSpeed = rSpeed;
			// Rotate the object around its local X axis at 1 degree per second
			transform.Rotate (Vector3.right * rotatingSpeed * Time.deltaTime);
		} else {
			rotatingSpeed = rSpeed;
			// Rotate the object around its local X axis at 1 degree per second
			transform.Rotate (Vector3.forward * rotatingSpeed * Time.deltaTime);
		}
	}
		
}
