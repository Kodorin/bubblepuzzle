﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour {

	public string resetSceneName;
	public string nextSceneName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//press R to reset the scene
		if(Input.GetKey(KeyCode.R)){
			SceneManager.LoadScene (resetSceneName);
		}

		//if the score reaches 0 transition to the next level
		if(PaddleController.score <= 0){
			SceneManager.LoadScene (nextSceneName);
		}
	}
}
