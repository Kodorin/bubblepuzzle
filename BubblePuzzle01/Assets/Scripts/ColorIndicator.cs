﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorIndicator : MonoBehaviour {

	public Material[] mat;

	// Use this for initialization
	void Start () {
		int chosenMaterial = Random.Range (0,mat.Length);
		GetComponent<Renderer>().material = mat [chosenMaterial];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
